#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include "calendarpreview.hpp"

#include <QtWidgets/QMainWindow>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    // QWidget interface
protected:
    void keyReleaseEvent(QKeyEvent *event) override;

private:
    CalendarPreview *preview_;
};
#endif // MAINWINDOW_HPP
