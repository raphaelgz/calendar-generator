#ifndef CALENDARDRAWER_HPP
#define CALENDARDRAWER_HPP

#include <QtGui/QPainter>

inline qreal mm_to_inch(qreal mm)
{
    return mm / 25.4;
}

//inline QSizeF mm_to_inch(const QSizeF &s)
//{
//    return s / 25.4;
//}

//inline QPointF mm_to_inch(const QPointF &p)
//{
//    return p / 25.4;
//}

//inline QRectF mm_to_inch(const QRectF &r)
//{
//    return QRectF(mm_to_inch(r.topLeft()), mm_to_inch(r.size()));
//}

inline qreal mm_to_point(int dpi, qreal mm)
{
    return mm_to_inch(mm) * dpi;
}

//inline QSizeF mm_to_point(int dpi, const QSizeF &s)
//{
//    return mm_to_inch(s) * dpi;
//}

//inline QPointF mm_to_point(int dpi, const QPointF &p)
//{
//    return mm_to_inch(p) * dpi;
//}

//inline QRectF mm_to_point(int dpi_x, int dpi_y, const QRectF &r)
//{
//    QRectF r_inch = mm_to_inch(r);

//    return QRectF(r_inch.left() * dpi_x, r_inch.top() * dpi_y, r_inch.right() * dpi_x, r_inch.bottom() * dpi_y);
//}

void draw_calendar(QPaintDevice *dev, int month, int year);

#endif // CALENDARDRAWER_HPP
