#include "calendar-drawer.hpp"

#include <QtCore/QDate>

static int get_day_of_week_column(int dow)
{
    return (dow % 7) + 1;
}

static QMarginsF calculate_margins(const QRectF &r, int percent)
{
    const qreal w = (r.width() / 100) * (percent / 2);
    const qreal h = (r.height() / 100) * (percent / 2);

    if(w < 1 || h < 1)
        return {};

    return QMarginsF(w, h, w, h);
}

static int calculate_font_size_for_rect(QFont font, const QString &text, int flags, const QRect &maxRect)
{
    int font_size = 12;
    int last_valid_font_size = -1;

    do {
        font.setPointSize(font_size);

        QFontMetrics metrics(font);

        auto br = metrics.boundingRect(maxRect, flags, text);

        if(br.size().width() > maxRect.size().width() || br.size().height() > maxRect.size().height()) {
            font_size--;
        } else {
            last_valid_font_size = font_size;

            if(br.size().width() < maxRect.size().width() && br.size().height() < maxRect.size().height())
                font_size++;
        }
    } while(last_valid_font_size < font_size || font_size == 0);

    if(last_valid_font_size == -1)
        return 12;

    return last_valid_font_size;
}

static void adjust_painter_font_size_for_rect(QPainter *p, const QString &text, int flags, const QRect &maxRect)
{
    QFont f = p->font();

    f.setPointSize(calculate_font_size_for_rect(f, text, flags, maxRect));

    p->setFont(f);
}

static void draw_weekday_name(QPainter *p, int dow, const QRectF &rect)
{
    static const QString week_day_names[] = {
        "DOMINGO",
        "SEGUNDA-FEIRA",
        "TERÇA-FEIRA",
        "QUARTA-FEIRA",
        "QUINTA-FEIRA",
        "SEXTA-FEIRA",
        "SÁBADO"
    };

    int font_size = 0;
    QFont painter_font = p->font();
    const Qt::Alignment alignment_flag = Qt::AlignCenter;

    for(const QString &name : week_day_names) {
        const int calculated_font_size = calculate_font_size_for_rect(p->font(), name, alignment_flag, rect.toRect());

        if(font_size == 0)
            font_size = calculated_font_size;
        else if(calculated_font_size < font_size)
            font_size = calculated_font_size;
    }

    painter_font.setPointSize(font_size);

    p->setFont(painter_font);
    p->drawText(rect, week_day_names[dow], alignment_flag);
}

static void draw_month_name(QPainter *p, int month, /*const QFont &font,*/ const QRectF &rect)
{
    static const QString month_names[] = {
        "JANEIRO",
        "FEVEREIRO",
        "MARÇO",
        "ABRIL",
        "MAIO",
        "JUNHO",
        "JULHO",
        "AGOSTO",
        "SETEMBRO",
        "OUTUBRO",
        "NOVEMBRO",
        "DEZEMBRO"
    };

    const QString &month_name = month_names[month - 1];
    const Qt::Alignment alignment_flag = Qt::AlignCenter;


//    p->setFont(font);
    adjust_painter_font_size_for_rect(p, month_name, alignment_flag, rect.toRect());

    p->setPen(QColor(89, 200, 201));
    p->drawText(rect, month_name, alignment_flag);
}

static void draw_year_number(QPainter *p, int year, const QRectF &rect)
{
    const QString year_text = QString::number(year);
    const Qt::Alignment alignment_flag = Qt::AlignCenter;

    adjust_painter_font_size_for_rect(p, year_text, alignment_flag, rect.toRect());

    p->setPen(Qt::lightGray);
    p->drawText(rect, year_text, alignment_flag);
}

static void draw_day_number(QPainter *p, int day, const QRectF &rect)
{
    const QString day_text = QString::number(day);
    const Qt::Alignment alignment_flag = Qt::AlignBottom | Qt::AlignRight;

    auto text_rect = QRectF(rect.center(), rect.bottomRight());

    adjust_painter_font_size_for_rect(p, day_text, alignment_flag, text_rect.toRect());

    p->drawText(text_rect, QString::number(day), alignment_flag);
}

void draw_calendar(QPaintDevice *dev, int month, int year)
{
    QPen borders_pen;

    borders_pen.setWidthF(mm_to_point(dev->logicalDpiX(), 1));
    borders_pen.setColor(Qt::black);

    QFont default_font("Arial");

    default_font.setBold(true);

    const int column_count = 8;
    const int row_count = 7;

    const QBrush column_colors[column_count] = {
        Qt::white,
        QColor(173, 129, 227),
        QColor(237, 160, 78),
        QColor(99, 173, 68),
        QColor(81, 204, 207),
        QColor(245, 118, 194),
        QColor(59, 139, 245),
        QColor(242, 242, 114)
    };

    if(dev->width() <= column_count || dev->height() <= row_count)
        return;

    QPainter painter(dev);

    const int cell_count = column_count * row_count;
    const QSizeF draw_area_size = QSizeF(mm_to_point(dev->logicalDpiX(), (dev->widthMM() * 0.96)),
                                         mm_to_point(dev->logicalDpiY(), (dev->heightMM() * 0.89)));
    const QSizeF cell_size = QSizeF(draw_area_size.width() / column_count, draw_area_size.height() / row_count);
    const QPointF draw_offset = QPointF((mm_to_point(dev->logicalDpiX(), dev->widthMM()) - draw_area_size.width()) / 2,
                                        (mm_to_point(dev->logicalDpiY(), dev->heightMM()) - draw_area_size.height()) / 2);

    for(int n = 0; n < cell_count; ++n) {
        const int current_row = n / column_count;
        const int current_column = n % column_count;

        const QRectF borders_rect = QRectF(QPointF(current_column * cell_size.width(), current_row * cell_size.height()) + draw_offset, cell_size);

        painter.setPen(borders_pen);
        painter.setFont(default_font);
        painter.setBrush(column_colors[current_column]);
        painter.drawRect(borders_rect);

        if(current_row == 0) {
            if(current_column > 0)
                draw_weekday_name(&painter, current_column - 1, borders_rect - calculate_margins(borders_rect, 10));
            else
                draw_month_name(&painter, month, borders_rect - calculate_margins(borders_rect, 10));
        } else if(current_row == 1) {
            if(current_column == 0)
                draw_year_number(&painter, year, borders_rect - calculate_margins(borders_rect, 10));
        }
    }

    int row_to_write = 0;

    for(auto d = QDate(year, month, 1); d.month() == month; d = d.addDays(1)) {
        if(d.day() == 1 || get_day_of_week_column(d.dayOfWeek()) == 1)
            row_to_write++;

        const QRectF borders_rect = QRectF(QPointF(get_day_of_week_column(d.dayOfWeek()) * cell_size.width(), row_to_write * cell_size.height()) + draw_offset, cell_size);

        draw_day_number(&painter, d.day(), borders_rect - calculate_margins(borders_rect, 25));
    }
}
