#include "calendarpreview.hpp"
#include "calendar-drawer.hpp"

#include <QtWidgets/QFileDialog>
#include <QtWidgets/QMessageBox>

#include <QtGui/QKeyEvent>
#include <QtGui/QWindow>
#include <QtGui/QImage>

#include <QtPrintSupport/QPrinter>
#include <QtPrintSupport/QPrintDialog>
#include <QtPrintSupport/QPrinterInfo>

CalendarPreview::CalendarPreview(QWidget *parent) : QWidget(parent)
{
    this->month_ = 1;
}

void CalendarPreview::nextMonth()
{
    if(this->month_ < 12)
        this->month_++;

    this->update();
}

void CalendarPreview::previousMonth()
{
    if(this->month_ > 1)
        this->month_--;

    this->update();
}

void CalendarPreview::print()
{
    QPrinter printer;

    QPrintDialog printer_dialog(&printer, this);

    if(printer_dialog.exec() != QPrintDialog::Accepted)
        return;

    draw_calendar(&printer, this->month_, 2020);
}

void CalendarPreview::save()
{
    QString filename = QFileDialog::getSaveFileName(this, "Save as image...", {}, "PNG Image (*.png)");

    if(filename.isEmpty())
        return;

    QImage img(4096, 2160, QImage::Format_ARGB32);

    draw_calendar(&img, this->month_, 2020);

    if(!img.save(filename))
        QMessageBox::critical(this, "Error", "Failed to save the file '" + filename + "'");
}

QSize CalendarPreview::sizeHint() const
{
    return QSize(mm_to_point(this->logicalDpiX(), 297), mm_to_point(this->logicalDpiY(), 210));
}

void CalendarPreview::paintEvent(QPaintEvent *event)
{
    draw_calendar(this, this->month_, 2020);
}

//void CalendarPreview::keyReleaseEvent(QKeyEvent *event)
//{
//    if(event->key() == Qt::Key::Key_Left) {
//        if(this->month_ > 1)
//            this->month_--;

//    } else if(event->key() == Qt::Key::Key_Right) {
//        if(this->month_ < 12)
//            this->month_++;
//    }

//    this->update();

//    QWidget::keyReleaseEvent(event);
//}
