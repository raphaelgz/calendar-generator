#ifndef CALENDARPREVIEW_HPP
#define CALENDARPREVIEW_HPP

#include <QtWidgets/QWidget>

class CalendarPreview : public QWidget
{
    Q_OBJECT

public:
    explicit CalendarPreview(QWidget *parent = nullptr);

    void nextMonth();
    void previousMonth();
    void print();
    void save();

    QSize sizeHint() const override;

protected:
    void paintEvent(QPaintEvent *event) override;
//    void keyReleaseEvent(QKeyEvent *event) override;

private:
    int month_;
};

#endif // CALENDARPREVIEW_HPP
