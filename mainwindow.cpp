#include "mainwindow.hpp"

#include <QtGui/QKeyEvent>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    this->preview_ = new CalendarPreview(this);
    this->setCentralWidget(this->preview_);
}

MainWindow::~MainWindow()
{
}

void MainWindow::keyReleaseEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key::Key_Left) {
        this->preview_->previousMonth();
    } else if(event->key() == Qt::Key::Key_Right) {
        this->preview_->nextMonth();
    } else if(event->modifiers() == Qt::Modifier::CTRL) {
        if(event->key() == Qt::Key::Key_P) {
            this->preview_->print();
        } else if(event->key() == Qt::Key::Key_S) {
            this->preview_->save();
        }
    }

    QMainWindow::keyReleaseEvent(event);
}

